require("dotenv").config();
const fs = require("fs");

const char = process.env.INPUT_CHAR;

if (char === undefined) {
  throw new Error(`
No character provided. Try pass to first argument or to env varible:
1) node ./src/index c
                    ^
                    |
                Your character
2) export INPUT_CHAR="c"
                      ^
                      |
                Your character
`);
}

function toUpper(text) {
  const splittedText = text.split("");
  return splittedText
    .map(
      /** @param {string} x **/
      (x) => {
        const charCode = x.charCodeAt(0);
        return charCode >= 97 && charCode <= 122
          ? String.fromCharCode(charCode - 32)
          : x;
      }
    )
    .join("");
}

const result = toUpper(char);

fs.writeFileSync(
  "./output.json",
  JSON.stringify({
    input: {
      char,
    },
    result,
  })
);
