# upChar

Converts a Latin letter to uppercase.

Support for arguments and env

Run:

```bash
# install modules
npm ci

# run
node ./src/index <arg>
```

Example .env:

```dotenv
INPUT_CHAR=a
```
